package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.InstrumentoMusical;
import ito.poo.clases.Presentaciones;

public class Myapp {

	static  void  run () {
		
		InstrumentoMusical Ins1 = new InstrumentoMusical ("Violin","Cuerdas",20);
		System.out.println (Ins1);
		
		Presentaciones p1 = new  Presentaciones ("Itzhak Perlman", LocalDate.of(2005, 1, 24));
		Presentaciones p2 = new  Presentaciones ("Itzhak Perlman", LocalDate.of(2010, 10, 04));
		Presentaciones p3 = new  Presentaciones ("Maxim Vengerov", LocalDate.of(2020, 6, 18));
		Presentaciones p4 = new  Presentaciones ("Sarah Chang", LocalDate.of(2000, 11, 15));
		Presentaciones p5 = new  Presentaciones ("Sarah Chang", LocalDate.of(2020, 9, 13));
		
		Ins1.addUsosxMusicos (p1);
		Ins1.addUsosxMusicos (p2);
		Ins1.addUsosxMusicos (p3);
		Ins1.addUsosxMusicos (p4);
		Ins1.addUsosxMusicos (p5);
		System.out.println (Ins1);
		
	}

	public  static  void  main ( String [] args ) {
		run();
	}
}
