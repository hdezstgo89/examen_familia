package ito.poo.clases;

import  java.util.ArrayList ;

public class InstrumentoMusical {

	private  String nombre = " ";
	private  String tipo = " ";
	private  int antiguedad = 0;
	private  ArrayList < Presentaciones > usadosxMusicos = new ArrayList < Presentaciones > ();
	
	public InstrumentoMusical() {
		super();
	}
	public InstrumentoMusical(String nombre, String tipo, int antiguedad) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		this.antiguedad = antiguedad;
	}
	/*************************************************/
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String newnombre) {
		this.nombre = newnombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String newtipo) {
		this.tipo = newtipo;
	}

	public int getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(int newantiguedad) {
		this.antiguedad = newantiguedad;
	}

	public ArrayList<Presentaciones> getusadosxMusicos() {
		return usadosxMusicos;
	}
	
	public  void  addUsosxMusicos (Presentaciones usadosxMusicos) {
		this.usadosxMusicos.add(usadosxMusicos);
	}
	@Override
	public String toString() {
		return "InstrumentoMusical [nombre=" + nombre + ", tipo=" + tipo + ", antiguedad=" + antiguedad
				+ ", usadosxMusicos=" + usadosxMusicos + "]";
	}
}
