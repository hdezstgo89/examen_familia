
package ito.poo.clases;

import java.time.LocalDate;

public class Presentaciones {

	private  String musico =  " ";
	private  LocalDate fecha =  null;
	
	public Presentaciones() {
		super();
	}
	
	public Presentaciones(String musico, LocalDate fecha) {
		super();
		this.musico = musico;
		this.fecha = fecha;
	}
	/*************************************************/
	public String getMusico() {
		return musico;
	}

	public void setMusico(String musico) {
		this.musico = musico;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Presentaciones [musico=" + musico + ", fecha=" + fecha + "]";
	}
}