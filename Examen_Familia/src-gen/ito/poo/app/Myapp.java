package ito.poo.app;

import ito.poo.clases.Familia;

public class Myapp {

	static void run () {
		
		System.out.println(Familia.getNumFamilias());
		Familia F1 = new Familia ("Samuel","Lucia");
		System.out.println (F1);
		System.out.println(Familia.getNumFamilias());
		F1.addHijos("Armando");
		System.out.println (F1);
		F1.addHijos("Rosio");
		System.out.println (F1);
		F1.addHijos("Rodrigo");
		System.out.println (F1);
		System.out.println(Familia.getNumFamilias());
		
		Familia F2 = new Familia ("Mario","Carmen");
		System.out.println (F2);
		System.out.println(Familia.getNumFamilias());
		F2.addHijos("Alejandro");
		System.out.println (F2);
		F2.addHijos("Jocelyn");
		System.out.println (F2);
		System.out.println(Familia.getNumFamilias());

		Familia F3 = new Familia ("Ricardo","Veronica");
		System.out.println (F3);
		System.out.println(Familia.getNumFamilias());
		F3.addHijos("Yahir");
		System.out.println (F3);
	}
	public  static  void  main ( String [] args ) {
		run();
	}
}

